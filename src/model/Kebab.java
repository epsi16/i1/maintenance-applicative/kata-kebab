package model;

import java.util.List;

public class Kebab {
    private String name;
    private List<Ingredient> ingredients;
    private List<Ingredient> sauces;


    public Kebab(String name, List<Ingredient> ingredients, List<Ingredient> sauces) {
        this.name = name;
        this.ingredients = ingredients;
        this.sauces = sauces;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isVege(){
        var isVege = true;
        var isFishLess = true;
        for( Ingredient ingredient : ingredients){
            if (ingredient.getType().equals("Viande")){
                isVege = false;
            }else if (ingredient.getType().equals("Fish")){
                isVege = false;
            }
        }
        return isVege;
    }

    public boolean isFishLess(){
        var isFishLess = true;
        for( Ingredient ingredient : ingredients){
            if (ingredient.getType().equals("Fish")){
                isFishLess = false;
            }
        }
        return isFishLess;
    }
    public boolean hasViande(){
        var hasViande = false;
        for( Ingredient ingredient : ingredients){
            if (ingredient.getType().equals("Viande")){
                hasViande = true;
            }
        }
        return hasViande;
    }

    @Override
    public String toString() {
        return name + "\n" +
                "ingredients :" + ingredients + "\n" +
                "sauces :" + sauces;
    }
}
