package model;

public class Ingredient {
    private String name;
    private String type;
    private int qte;

    public Ingredient(String name, String type) {
        this.name = name;
        this.type = type;
        this.qte = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    @Override
    public String toString() {
        return "Ingredient :" +
                name + '\'' +
                ", qte(" + qte + ") \n";
    }
}
