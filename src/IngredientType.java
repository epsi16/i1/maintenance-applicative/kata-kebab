public enum IngredientType {
    VIANDE,
    POISSON,
    FROMAGE,
    LEGUME,
    SAUCE,
}
