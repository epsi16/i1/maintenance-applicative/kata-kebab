import model.Ingredient;
import model.Kebab;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Ingredient> ingredients = generateIngredients();
        System.out.println("Liste des Ingrédients :");
        System.out.println("------------------------");
        for (Ingredient ingredient : ingredients) {
            System.out.println((ingredients.indexOf(ingredient) + 1) + "- " + ingredient.getName());
        }
        int choice;

        List<Ingredient> ingredientsKebab = new ArrayList<>();
        List<Ingredient> saucesKebab = new ArrayList<>();

        do {
            System.out.println("Veuillez choisir vos ingrédients pour passer au choix des sauces sur la touche 0");
            choice = scanner.nextInt();
            if (choice != 0) {
                System.out.println("Veuillez choisir la quantité");
                int qte = scanner.nextInt();
                ingredients.get(choice - 1).setQte(qte);
                ingredientsKebab.add(ingredients.get(choice - 1));
                ingredients.remove(choice - 1);
            }
        } while (choice != 0);

        List<Ingredient> sauces = generateSauce();
        System.out.println("Liste des Sauces :");
        System.out.println("------------------------");
        for (Ingredient sauce : sauces) {
            System.out.println((sauces.indexOf(sauce) + 1) + "- " + sauce.getName());
        }

        do {
            choice = 100;
            System.out.println("Veuillez choisir vos sauces pour terminer votre kebab appuyer sur la touche 0");
            choice = scanner.nextInt();
            if (choice != 0) {
                System.out.println("Veuillez choisir la quantité");
                int qte = scanner.nextInt();
                sauces.get(choice - 1).setQte(qte);
                saucesKebab.add(sauces.get(choice - 1));
                sauces.remove(choice - 1);
            }
        } while (choice != 0);

        Kebab kebab = new Kebab("kabab", ingredientsKebab, saucesKebab);

        var isFishLess = kebab.isFishLess();

        if (kebab.hasViande() == true) {
            System.out.println("Votre Kebab n'est ni pescitarien ni végé");
        } else if (isFishLess == false){
            System.out.println("Votre Kebab est pescitarien");
        }else{
            System.out.println("Votre Kebab est végé");
        }
        System.out.println(kebab);
    }

    private static List<Ingredient> generateIngredients() {
        Ingredient Döner = new Ingredient("Döner", "Viande");
        Ingredient Poulet = new Ingredient("Poulet", "Viande");
        Ingredient salade = new Ingredient("salade", "Legume");
        Ingredient oignon = new Ingredient("oignon", "Legume");
        Ingredient tomate = new Ingredient("tomate", "Legume");
        Ingredient crevette = new Ingredient("crevette", "Fish");
        Ingredient poisson = new Ingredient("poisson", "Fish");
        Ingredient Surimi = new Ingredient("Surimi", "Fish");
        Ingredient cheddar = new Ingredient("cheddar", "Fromage");
        Ingredient raclette = new Ingredient("raclette", "Fromage");
        Ingredient maroilles = new Ingredient("maroilles", "Fromage");
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(Döner);
        ingredients.add(Poulet);
        ingredients.add(salade);
        ingredients.add(oignon);
        ingredients.add(tomate);
        ingredients.add(crevette);
        ingredients.add(poisson);
        ingredients.add(Surimi);
        ingredients.add(cheddar);
        ingredients.add(raclette);
        ingredients.add(maroilles);
        return ingredients;
    }

    private static List<Ingredient> generateSauce() {
        Ingredient blanche = new Ingredient("blanche", "Sauce");
        Ingredient béchamel = new Ingredient("béchamel", "Sauce");
        Ingredient algerienne = new Ingredient("algérienne", "Sauce");
        Ingredient ketchup = new Ingredient("ketchup", "Sauce");
        Ingredient mayo = new Ingredient("mayo", "Sauce");
        Ingredient moutarde = new Ingredient("moutarde", "Sauce");
        Ingredient barbecue = new Ingredient("barbecue", "Sauce");
        Ingredient samourai = new Ingredient("samourai", "Sauce");
        List<Ingredient> sauces = new ArrayList<>();
        sauces.add(blanche);
        sauces.add(béchamel);
        sauces.add(algerienne);
        sauces.add(ketchup);
        sauces.add(mayo);
        sauces.add(moutarde);
        sauces.add(barbecue);
        sauces.add(samourai);
        return sauces;
    }
}
